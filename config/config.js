/*jslint node: true */
/*jslint white: true */
(function() {
    'use strict';
    module.exports = {
    	'domain': process.env.DOMAIN || 'localhost',
        'port': process.env.PORT || 10000,
        'secret': 'ItsMyWayOfLivingSoJustDontComeInMyWay',
        'environment':process.env.ENVIRONMENT ||'test'
    };
}());