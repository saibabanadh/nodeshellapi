var express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	config = require('./config/config');

var terminal=require('node-cmd');
//var exec=require('exec');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var applicationUrl = 'http://' + config.domain + ':' + config.port;


app.use(bodyParser.urlencoded({limit: '20mb',extended: true}));
app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.json({limit: '20mb',type:'application/vnd.api+json'}));

var session = require('express-session'); 
app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: config.secret
}));

//SECURITY 
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type, Authorization');
    next();
});

//ROUTES
app.get('/',function(req,res){
	res.send("Hi, Its still under construction");
});

app.post('/command',function(req,res){
	console.log(req.body.command);
	var command = req.body.command.split(' ');
	console.log(command);
	exec(command, function(err, out, code) {
	  if (err instanceof Error)
	    throw err;
	  process.stderr.write(err);
	  process.stdout.write(out);
	  //process.exit(code);
	  res.json({res:out})
	});
});

app.post('/deploy',function(req,res){
	exec('test.sh', function(error,stdout,stderr){
		if(error) 
			console.log(error);
		console.log(stderr);
		console.log(stdout);

	})
});


// UNKNOWN ROUTES
app.use('*',function(req, res) {
    res.status(404).json({error:"requested url: "+req.url+ ' is Not Found'});
});

// START THE SERVER
app.listen(config.port, function(){
    console.log('Server is running at ==> ' + applicationUrl);    
});
   
module.exports=app;

// app.post('/command',function(req,res){
// 	console.log(req.body.command);
// 	var command = req.body.command;
// 	terminal.get(command,function(data){
//         res.json({result:data})
//     });
// });
